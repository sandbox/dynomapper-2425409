CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------
The DYNO Mapper Login component guarantees that DYNO Mapper can authenticate
and crawl both public and private pages of any Drupal website. 


* For a full description of the module, visit the sandbox page:
   https://www.drupal.org/sandbox/dynomapper/2425409


* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2425409




REQUIREMENTS
------------
This module requires the a DYNO Mapper monthly paid subscription. 




INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.




CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will allow for authentication with DYNO Mapper. 




MAINTAINERS
-----------
Current maintainers:
* Garenne Bigby (dynomapper) - https://www.drupal.org/user/3158107
